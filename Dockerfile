FROM alpine:latest

# Install dependencies
RUN apk add --no-cache ansible \
    py3-pip \
    openssh-client \
    git

# Install paramiko
RUN pip3 install --upgrade pip
RUN pip3 install paramiko


RUN mkdir -p /root/.ssh

RUN echo -e "Host *\n\tStrictHostKeyChecking no" > /root/.ssh/config
COPY Ansible/ansible.cfg /etc/ansible/ansible.cfg


RUN git config --global user.email "anurag@he.net"
RUN git config --global user.name "Anurag Bhatia"


RUN rm -rf /root/.cache
